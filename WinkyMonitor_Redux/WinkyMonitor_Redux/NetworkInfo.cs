﻿using System;
using System.Net.NetworkInformation;

namespace WinkyMonitor_Redux
{
    class NetworkInfo
    {
        public string bytesRecieved = "";
        public string bytesSent = "";
        public bool isNetworkUp = false;
        

        private double upLoadTotal, upLoadOld = 0,
            downLoadTotal, downLoadOld = 0, downLoadNew, upLoadNew;

        public void NetworkUsage()
        {
            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            //Grabs all the needed info for network usage
            //NetworkInterface ni = interfaces[3];
            foreach (var item in interfaces)
            {
                NetworkInterface ni = item;

                if (NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    if (ni.OperationalStatus == OperationalStatus.Up)
                    {
                        GetNetworkDownloadSpeed(ni);
                        GetNetworkUploadSpeed(ni);
                        isNetworkUp = true;
                        return;
                    }
                }
            }

            isNetworkUp = false;

        }

        private void GetNetworkDownloadSpeed(NetworkInterface ni)
        {
            downLoadNew = (ni.GetIPv4Statistics().BytesReceived / 131072.0);
            downLoadTotal = downLoadNew - downLoadOld;
            bytesRecieved = "";

            FormatNetworkRealTimeSpeed(downLoadTotal, ref bytesRecieved, downLoadNew,ref downLoadOld, false);
        }

        private void GetNetworkUploadSpeed(NetworkInterface ni)
        {
            upLoadNew = (ni.GetIPv4Statistics().BytesSent / 131072.0);
            upLoadTotal = upLoadNew - upLoadOld;
            bytesSent = "";

            FormatNetworkRealTimeSpeed(upLoadTotal, ref bytesSent, upLoadNew, ref upLoadOld, true);
        }

        private void FormatNetworkRealTimeSpeed(double bytesTotal, ref string bytesFinal, double bytesNew, ref double bytesOld, bool isUpload)
        {
            if (bytesTotal > 1)
            {
                bytesFinal = Convert.ToDouble(bytesTotal).ToString("f2") + "M";       
            }
            else
            {
                bytesFinal = Convert.ToDouble(bytesTotal * 1024).ToString("f2") + "K";
            }

            bytesOld = bytesNew;
        }
    }
}
