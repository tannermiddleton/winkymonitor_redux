﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinkyMonitor_Redux
{
    class RAMInfo
    {
        public double GetRAMUsage()
        {
            double dblRamFree;
            double dblRamTotal;
            double dblRamUsed;

            dblRamFree = new Microsoft.VisualBasic.Devices.ComputerInfo().AvailablePhysicalMemory / 1073741824.004733;
            dblRamTotal = new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory / 1073741824.004733;
            dblRamUsed = dblRamTotal - dblRamFree;
            return dblRamUsed / dblRamTotal * 100;
        }
    }
}
