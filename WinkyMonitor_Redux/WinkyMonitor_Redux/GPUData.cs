﻿using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinkyMonitor_Redux
{
    class GPUData
    {
        #region Variables
        public static double GPUTemp = 0;
        public double GPULoad = 0;
        public static double GPUFan = 0;
        public static double GPUClock = 0;
        public static double GPUMemory = 0;
        public string FormattedGPUInfo;
        private static Computer hwInfo; 
        #endregion

        public void StartHardWareMonitor()
        {
            hwInfo = new Computer();
            hwInfo.GPUEnabled = true;
            hwInfo.Open();
        }

        public void GPU_Load()
        {
            GPULoad = 0;
            if (hwInfo.Hardware.Count() > 0)
            {
                hwInfo.Hardware[0].Update();
                
                GPULoad = GetValue("GPU CORE", "LOAD");
            }
        }

        public void GPU_Info()
        {
            GPUData.GPUTemp = GetValue("GPU CORE", "TEMPERATURE");
            GPUData.GPUFan = GetValue("GPU FAN", "FAN");
            GPUData.GPUMemory = GetValue("GPU MEMORY", "CLOCK");
            GPUData.GPUClock = GetValue("GPU CORE", "CLOCK");

            FormattedGPUInfo = string.Format(" GPU TEMP: {0}° \r\n GPU Fan Speed: {1}rpm \r\n" +
                " GPU Clock: {2}Mhz \r\n GPU Memory: {3}Mhz \r\n", GPUData.GPUTemp.ToString("F2"),
                GPUData.GPUFan.ToString("N0"), GPUData.GPUClock.ToString("N0"), GPUData.GPUMemory.ToString("N0"));
        }

        public double GetValue(string sensorName, string sensorType)
        {
            try
            {
                float value = 0;

                float? t = (from tt in hwInfo.Hardware[0].Sensors
                            where tt.SensorType.ToString().ToUpper() == sensorType
                            && tt.Name.ToString().ToUpper() == sensorName
                            select tt.Value).First();
                value = (float)t;

                return value;
            }
            catch (Exception)
            {
                //attribute isn't available on GPU
                return 0;
            }
        }
    }
}
