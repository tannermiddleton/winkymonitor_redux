﻿using NSpeedTest;
using NSpeedTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WinkyMonitor_Redux
{
    class NetworkSpeedTest
    {
        private static SpeedTestClient svcSpeedTest;
        private static Settings settings;
        public string strDownload = "";
        public string strUpload = "";
        public bool testingDownload = false;
        public bool testingUpload = false;
        public int Ping { get; private set; }

        public async Task RunNetworkSpeedTest()
        {
            svcSpeedTest = new SpeedTestClient();

            try
            {
                await Task.Run(() =>
                {
                    settings = svcSpeedTest.GetSettings();
                    var servers = SelectServers();
                    var bestServer = SelectBestServer(servers);

                    Ping = svcSpeedTest.TestServerLatency(bestServer, 50);

                    TestDownload(bestServer);                  
                    TestUpload(bestServer);
                });
            }
            finally
            {
                svcSpeedTest = null;
                GC.Collect();
            }
        }

        private void TestDownload(Server bestServer)
        {
            double dblDownload = 0;
            testingDownload = true;
            dblDownload = svcSpeedTest.TestDownloadSpeed(bestServer, settings.Download.ThreadsPerUrl, 2);

            FormatSpeeds(ref dblDownload, ref strDownload);
            testingDownload = false;
        }

        private void TestUpload(Server bestServer)
        {
            double dblUpload = 0;
            testingUpload = true;
            dblUpload = svcSpeedTest.TestUploadSpeed(bestServer, settings.Upload.ThreadsPerUrl, 1);

            FormatSpeeds(ref dblUpload, ref strUpload);
            testingUpload = false;
        }

        private void FormatSpeeds(ref double NetSpeed, ref string NetVariable)
        {
            if (NetSpeed > 1024)
            {
                NetSpeed = Math.Round(NetSpeed / 1024, 2);
                NetVariable = NetSpeed.ToString() + "Mbps";
            }
            else
            {
                NetSpeed = Math.Round(NetSpeed, 2);
                NetVariable = NetSpeed.ToString() + "Kbps";
            }
        }

        private static Server SelectBestServer(IEnumerable<Server> servers)
        {
            var bestServer = servers.OrderBy(x => x.Latency).First();
            return bestServer;
        }

        private static IEnumerable<Server> SelectServers()
        {
            var servers = settings.Servers.Where(s => s.Country.Equals("United States")).Take(10).ToList();

            foreach (var server in servers)
            {
                server.Latency = svcSpeedTest.TestServerLatency(server);
            }
            return servers;
        }

    }
}
