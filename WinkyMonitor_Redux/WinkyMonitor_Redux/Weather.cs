﻿using System;
using System.Device.Location;
using System.Text;
using System.Threading.Tasks;


namespace WinkyMonitor_Redux
{
    class Weather
    {
        #region Variables
        public string Condition { get; private set; }
        public int Temp { get; private set; }
        public string Alerts { get; private set; }
        public string WeatherTitle { get; private set; }
        public string Humidity { get; private set; }
        public string Icon { get; private set; }
        public string Wind { get; private set; }
        public bool NewLocation { get; private set; }
        public string StormDistance { get; private set; }
        public string City { get; private set; }
        public int FeelsLikeTemp { get; private set; }
        public bool LocationUnknown { get; private set; } = true;
        public int WindGust { get; private set; }
        public double Lat;
        public double Lon;
        public string FormattedConditions { get; private set; }
        private GeoCoordinateWatcher watcher; 
        #endregion

        public void GetLocation()
        {
            watcher = new System.Device.Location.GeoCoordinateWatcher(GeoPositionAccuracy.High);
            watcher.PositionChanged += Watcher_PositionChanged;

            watcher.TryStart(false, TimeSpan.FromMilliseconds(200));           
        }

        private void Watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (Lat == 0)
            {
                NewLocation = true;
            }
            else
            {
                NewLocation = false;
            }

            Lat = e.Position.Location.Latitude;
            Lon = e.Position.Location.Longitude;     
        }

        public async Task GetWeatherInfo()
        {
            string summaryToday = "";
            string summaryTomorrow = "";
            if (Lat != 0 && Lon != 0)
            {

                WeatherNet.ClientSettings.SetApiKey("83d1de09463322d54e4131bf9e3de762");
                WeatherNet.Clients.CurrentWeather currentWeather = new WeatherNet.Clients.CurrentWeather();

                var result = WeatherNet.Clients.CurrentWeather.GetByCoordinates(Lat, Lon);

                if (result.Item.Icon != "")
                {
                    Icon = result.Item.Icon;
                }
                WeatherTitle = result.Item.Title;

                Alerts = null;
                DarkSky.Services.DarkSkyService svcDarkSky = new DarkSky.Services.DarkSkyService("0323e11fd686f69c4cebe307b31557cf");

                var dsResponse = await svcDarkSky.GetForecast(Lat, Lon);

                if (dsResponse.Response.Alerts != null)
                {
                    StringBuilder alerts = new StringBuilder();

                    foreach (var item in dsResponse.Response.Alerts)
                    {

                        alerts.Append("Region: " + result.Item.City);
                        alerts.AppendLine();
                        alerts.AppendLine();
                        alerts.Append("Type: " + item.Title);
                        alerts.AppendLine();
                        alerts.AppendLine();
                        alerts.Append("Expires: " + item.ExpiresDateTime);
                        alerts.AppendLine();
                        alerts.AppendLine();
                        alerts.Append("Severity: " + item.Severity);
                        alerts.AppendLine();
                        alerts.AppendLine();
                        alerts.Append(item.Description);

                    }

                    Alerts = alerts.ToString().Replace("...", " ");
                }


                summaryToday = dsResponse.Response.Daily.Data[0].Summary;
                summaryTomorrow = dsResponse.Response.Daily.Data[1].Summary;
                FeelsLikeTemp = Convert.ToInt32(dsResponse.Response.Currently.ApparentTemperature);
                Temp = Convert.ToInt32(dsResponse.Response.Currently.Temperature);
                WindGust = Convert.ToInt32(dsResponse.Response.Currently.WindGust);
                Wind = dsResponse.Response.Currently.WindSpeed.ToString();
                Condition = dsResponse.Response.Currently.Summary;
                City = result.Item.City;
                Humidity = (dsResponse.Response.Currently.Humidity * 100).ToString() + "%";
                FormattedConditions = string.Format(" City: {11} \r\n Summary: {5} \r\n Current Condition: {4} \r\n High: {9}° Low: {10}° \r\n Feels Like: {0}° \r\n Humidity: {1}\r\n Wind Speed: {2}Mph \r\n Wind Gusts Of: {3}Mph \r\n \r\n Tomorrow: {6} \r\n High: {7}° Low: {8}° \r\n ", FeelsLikeTemp.ToString(), Humidity,
                  Wind, WindGust.ToString(), Condition, summaryToday, summaryTomorrow, dsResponse.Response.Daily.Data[1].TemperatureHigh, dsResponse.Response.Daily.Data[1].TemperatureLow,
                  dsResponse.Response.Daily.Data[0].TemperatureHigh, dsResponse.Response.Daily.Data[0].TemperatureLow,result.Item.City);
            }
        }
    }
}
