﻿using System;
using System.Diagnostics;
using System.Management;
using HardwareProviders.CPU;
using OpenHardwareMonitor.Hardware;

namespace WinkyMonitor_Redux
{
    class CPUInfo
    {
        private PerformanceCounter cpuCounter = new PerformanceCounter();
        public string cpuTemp = "";
        public string cpuVoltage = "";
        public string packagePower = "";
        public string cpuFreq = "";
        public string cpuModel = "";
        public double cpuTempSum = 0.00;
        public double cpuTempAverage = 0.00;
        private int cpuAverageCounter = 0;
        public string cpuAverage = "Gathering Data" +
            "";
        public double CPU_Usage()
        {
           

            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            return cpuCounter.NextValue();
        }

        public void CPU_Info()
        {
            var cpu = Cpu.Discover();

            if (cpu != null)
            {
                if (cpu[0].Vendor.ToString().ToUpper().Contains("INTEL"))
                {
                    cpuModel = cpu[0].Name;
                    cpuTemp = Convert.ToDouble(((Cpu)cpu[0]).PackageTemperature.Value).ToString("N2") + " C";
                    cpuFreq = (Convert.ToDouble(cpu[0].TimeStampCounterFrequency) / 1000).ToString("N2");

                    //
                    // CPU Voltage not working. It's 3AM and I give up for now.
                    //
                    //cpuVoltage = Convert.ToDouble(((HardwareProviders.CPU.IntelCpu)intel[0]).Processor.CoreVoltage.Value).ToString("N2");
                }
                else if (cpu[0].Name.ToUpper().Contains("RYZEN")) // AMD Ryzen CPUs
                {
                    cpuModel = cpu[0].Name;
                    cpuTemp = Convert.ToDouble(((AmdCpu17)cpu[0]).Processor.CoreTemperatureTdie.Value).ToString("N2");
                    cpuVoltage = Convert.ToDouble(((AmdCpu17)cpu[0]).Processor.CoreVoltage.Value).ToString("N3");
                    cpuFreq = (Convert.ToDouble(cpu[0].TimeStampCounterFrequency) / 1000).ToString("N2");
                }
                else
                {
                    cpuModel = cpu[0].Name;
                    cpuFreq = (Convert.ToDouble(cpu[0].TimeStampCounterFrequency) / 1000).ToString("N2");
                    cpuTemp = Convert.ToDouble(((AmdCpu)cpu[0]).CoreTemperatures[0].Value).ToString("N2") + " C";

                    //ensure voltage sensor has elements in it's array
                    if (cpu[0].CoreVoltages.Length > 0)
                    {
                        cpuVoltage = Convert.ToDouble(cpu[0].CoreVoltages[0].Value).ToString("N3");
                    }
                    else
                    {
                        //voltage reading not supported
                        cpuVoltage = "0.00";
                    }
                }

                cpuAverageCounter++;
                if (cpuTemp != "" && cpuAverageCounter < 30)
                {
                    cpuTempSum = cpuTempSum + Convert.ToDouble(cpuTemp);

                    if (cpuAverage.ToUpper().Contains("GATHERING"))
                    {
                        if (cpuAverage.Contains("..."))
                        {
                            cpuAverage = "Gathering Data";
                        }
                        else
                        {
                            cpuAverage = cpuAverage + ".";
                        }
                    }
                }
                else
                {
                    cpuTempAverage = cpuTempSum / cpuAverageCounter;
                    cpuAverage = cpuTempAverage.ToString("F2") + "°";
                    cpuAverageCounter = 0;
                    cpuTempSum = 0;
                }
            }
        }
    }
}
