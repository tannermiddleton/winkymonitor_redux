﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;


namespace WinkyMonitor_Redux
{
    public static class ProgressBarExtensions
    {
        private static TimeSpan duration = TimeSpan.FromSeconds(0.1);

        public static void SetPercent(this ProgressBar progressBar, double percentage)
        {
            DoubleAnimation animation = new DoubleAnimation(percentage, duration);
            progressBar.BeginAnimation(ProgressBar.ValueProperty, animation);
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RAMInfo clsRAM = new RAMInfo();
        CPUInfo CPU = new CPUInfo();
        GPUData GPU = new GPUData();
        NetworkInfo Network = new NetworkInfo();
        NetworkSpeedTest speedTest = new NetworkSpeedTest();
        DriveSpace Drives = new DriveSpace();
        Weather Weather = new Weather();
        public SeriesCollection SeriesCollectionCPU { get; set; }
        public SeriesCollection SeriesCollectionGPU { get; set; }

        //Setup Line Series
        public MainWindow()
        { 
            InitializeComponent();

            ToolTipService.ShowDurationProperty.OverrideMetadata(
            typeof(DependencyObject), new FrameworkPropertyMetadata(Int32.MaxValue));

            Color BaseColor = (Color)ColorConverter.ConvertFromString("#35b5e5");
            SolidColorBrush brush = new SolidColorBrush(BaseColor);
            
            SeriesCollectionCPU = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "CPU",
                    Values = new ChartValues<double> { 100,0 },
                    PointGeometrySize = 0,
                    Fill = brush,
                    StrokeThickness = 0,
                    LineSmoothness = 1
                }
            };
            SeriesCollectionGPU = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "GPU",
                    Values = new ChartValues<double> {  100, 0 },
                    PointGeometrySize = 0,
                    Fill = brush,
                    StrokeThickness = 0
                }
            };

            cpuChart.DataTooltip = null;
            GPUChart.DataTooltip = null;
            DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetNetwork_Usage();
            GPU.StartHardWareMonitor();
            GetCPU_Usage();
            GetCPU_Info();
            GetGPU_Info();
            GetRAM_Usage();
            GetDriveInfo();

            if (Network.isNetworkUp)
            {
                Weather.GetLocation();
                GetWeather();
            }
            else
            {
                txtWeather.Text = "Network Down";
                txtWeather.FontSize = 14;
            }
        }

        #region System_Usage

        private async void GetRAM_Usage()
        {
            double RamUsage = 0;

            while (true)
            {
                await Task.Run(() =>
                {
                    RamUsage = clsRAM.GetRAMUsage();
                    Thread.Sleep(2000);
                });

                lblRamUsed.Content = RamUsage.ToString("N0") + "%";
                progRAM.SetPercent(RamUsage);

                SetControlColor(ref progRAM, ref lblRamUsed, RamUsage);
            }
        }

        private async void GetGPU_Info()
        {
            double GPULoadAverage;
            while (true)
            {
                GPULoadAverage = 0;
                await Task.Run(() =>
                {
                    for (int i = 0; i < 50; i++)
                    {
                        GPU.GPU_Load();
                        GPULoadAverage += GPU.GPULoad;
                        Thread.Sleep(20);
                    }

                    GPULoadAverage = GPULoadAverage / 50;
                    GPU.GPU_Info();
                });

                lblGPUUsage.Content = GPULoadAverage.ToString("N0") + "%";
                progGPU.SetPercent(GPULoadAverage);

                SetControlColor(ref progGPU, ref lblGPUUsage, GPULoadAverage);
                DrawHistogram(SeriesCollectionGPU, GPULoadAverage);

                gpuSummary.Text = GPU.FormattedGPUInfo;
            }
        }

        private async void GetCPU_Usage()
        {
            double CPU_Usage = 0;

            while (true)
            {
                await Task.Run(() =>
                {
                    CPU_Usage = CPU.CPU_Usage();
                    Thread.Sleep(1000);
                });

                DrawHistogram(SeriesCollectionCPU, CPU_Usage);
                progCPU.SetPercent(CPU_Usage);
                lblCPUNum.Content = CPU_Usage.ToString("N0") + "%";

                SetControlColor(ref progCPU, ref lblCPUNum, CPU_Usage);
            }
        }

        private async void GetCPU_Info()
        {

            try
            {
                while (true)
                {
                    await Task.Run(() =>
                    {
                        CPU.CPU_Info();
                        Thread.Sleep(1000);
                    });

                    cpuSummary.Text = string.Format(" CPU Model: {3} \r\n CPU Temp: {0}° \r\n CPU Avg Temp: {4} \r\n CPU Voltage: {1}v \r\n CPU Frequency: {2}Ghz \r\n",
                        CPU.cpuTemp, CPU.cpuVoltage, CPU.cpuFreq, CPU.cpuModel,CPU.cpuAverage);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace);
            }
        }

        private async void GetNetwork_Usage()
        {
            while (true)
            {
                await Task.Run(() =>
                {
                    Network.NetworkUsage();
                    Thread.Sleep(1000);
                });

                if (txtWeather.Text.Contains("Network") && Network.isNetworkUp)
                {
                    Weather.GetLocation();
                    GetWeather();
                    txtWeather.FontSize = 20;
                }

                if (Weather.NewLocation)
                {
                    imgIcon_MouseRightButtonUp(null,null);
                }

                SetNetworkColors();

                txtRecieving.Text = Network.bytesRecieved;
                txtSending.Text = Network.bytesSent;
            }
        }

        private async void GetDriveInfo()
        {
            double DriveSpace = 0;

            while (true)
            {
                List<KeyValuePair<string, string>> disks = new List<KeyValuePair<string, string>>();

                await Task.Run(() =>
                {
                    disks = Drives.GetDriveUsage();
                    Drives.GetDiskReadRate();
                    Drives.GetDiskWriteRate();
                    Thread.Sleep(1000);
                });

                double mainDriveAvailableSpace = Convert.ToDouble(disks[0].Value.Split(',')[1]);
                double mainDriveTotalSpace = Convert.ToDouble(disks[0].Value.Split(',')[0]);

                DriveSpace = (mainDriveAvailableSpace / mainDriveTotalSpace) * 100;
                double driveFreePercent = 100 - DriveSpace;

                lblDriveFree.Content = driveFreePercent.ToString("N0") + "%";
                progDisk.SetPercent(driveFreePercent);

                driveSummary.Text = string.Format(" Read Speed: {0}MB/s" + Environment.NewLine + " Write Speed: {1}MB/s \r\n",
                     Drives.dblRead.ToString("N2"), Drives.dblWrite.ToString("N2"));

                foreach (var item in disks)
                {
                    driveSummary.Text += " \r\n Disk " + item.Key.ToString().Trim('\\') + " " + Convert.ToDouble(item.Value.Split(',')[1]).ToString("N2") + "GB of free space";
                }

                driveSummary.Text += Environment.NewLine;

                disks.Clear();
                SetControlColor(ref progDisk, ref lblDriveFree, driveFreePercent);
            }
        }
        #endregion

        #region Weather Info
        private async void GetWeather()
        {
            while (true)
            {
                await Weather.GetWeatherInfo();
                txtWeather.Text = Weather.Temp.ToString().TrimEnd('.') + "°";
                SetWeatherIcon();
                GC.Collect();
                await Task.Run(() =>
                {
                    Thread.Sleep(900000);
                });
            }
        }

        private void SetWeatherIcon()
        {
            if (Weather.Icon != null && Weather.Icon != "")
            {
                ImageSource img = new BitmapImage(new Uri(@"http://openweathermap.org/img/w/" + Weather.Icon + ".png"));
                imgIcon.Source = img;
                txtSummary.Text = Weather.FormattedConditions;
            }

            if (Weather.Alerts != null)
            {

                ImageSource imgalert = new BitmapImage(new Uri(@"https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Error_warning_alert_attention_remove_dialog.png"));
                imgAlertIcon.Source = imgalert;
                txtToolTip.Text = "";

                foreach (var item in Weather.Alerts.Split('*'))
                {
                    txtToolTip.Text += item.TrimStart(' ');
                }

            }
        }

        private async void imgIcon_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            txtWeather.Text = "🔁";
            txtWeather.FontSize = 20;
        
            await Weather.GetWeatherInfo();
            txtWeather.FontSize = 20;
            txtWeather.Text = Weather.Temp.ToString().TrimEnd('.') + "°";
            SetWeatherIcon();
        }

        private void imgAlertIcon_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.wunderground.com/wundermap?lat=" + Weather.Lat.ToString() + "&lon=" + Weather.Lon.ToString());
        } 
        #endregion

        #region SetColorRegion
        private void SetControlColor(ref ProgressBar progControl, ref Label labelControl, double currentUsage)
        {
            if (currentUsage < 50)
            {
                labelControl.Foreground = Brushes.LawnGreen;
                progControl.Foreground = Brushes.LawnGreen;
            }
            else if (currentUsage < 75)
            {
                labelControl.Foreground = Brushes.Yellow;
                progControl.Foreground = Brushes.Yellow;
            }
            else
            {
                labelControl.Foreground = Brushes.Orange;
                progControl.Foreground = Brushes.Orange;
            }
        }

        private void SetNetworkColors()
        {
            if (speedTest.testingDownload)
            {
                lblDownloading.Foreground = Brushes.White;
                lblDownloading.Foreground = Brushes.LawnGreen;
            }
            else if (speedTest.testingUpload)
            {
                lblDownloading.Foreground = Brushes.White;
                lblUploading.Foreground = Brushes.LawnGreen;
            }
            else
            {
                lblDownloading.Foreground = Brushes.White;
                lblUploading.Foreground = Brushes.White;
            }
        }
        #endregion

        //Start Network Speedtest
        private async void lblUploading_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Network.isNetworkUp)
            {
                speedTest.testingDownload = true;
                speedtestSummary.Text = "Speedtest Active";
                await speedTest.RunNetworkSpeedTest();

                speedtestSummary.Text = "Download Speed: " + speedTest.strDownload + Environment.NewLine;
                speedtestSummary.Text += "Upload Speed: " + speedTest.strUpload + Environment.NewLine;
                speedtestSummary.Text += "Ping: " + speedTest.Ping.ToString() + "ms" + Environment.NewLine;
            }
        }
        private async void DrawHistogram(SeriesCollection collection, double Value)
        {
            await Task.Run(() =>
            {
                collection[0].Values.Add(Value);

                if (collection[0].Values.Count > 30)
                {
                    collection[0].Values.RemoveAt(1);
                }
            }); 
        }
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.Topmost = !this.Topmost;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString().ToUpper() == "ESCAPE")
            {
                if (MessageBox.Show("Are you sure you want to close WinkyMonitor?", "Exit Application", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }

            if (e.Key.ToString().ToUpper() == "W")
            {
                System.Diagnostics.Process.Start("https://www.wunderground.com/wundermap?lat=" + Weather.Lat.ToString() + "&lon=" + Weather.Lon.ToString());
            }
        }

        private async void progDisk_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            await Task.Run(() =>
            {
                Drives.DiskSpeedTest();
            });
            
        }

        private void GPUChart_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void cpuChart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("Taskmgr");
        }

        private void progDisk_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("diskmgmt.msc");
        }
    }
}
