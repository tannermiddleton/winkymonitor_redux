﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Microsoft.AspNetCore.WebUtilities;

namespace WinkyMonitor_Redux
{
    class DriveSpace
    {
        DriveInfo[] drives = DriveInfo.GetDrives();
        public double dblDriveTotal;
        public double dblDriveSpace;
        public double dblRead { get; private set; }
        public double dblWrite { get; private set; }
        
        public List<KeyValuePair<string,string>> drivesList = new List<KeyValuePair<string, string>>();
        private PerformanceCounter drivePerfReadCounter = new PerformanceCounter();
        private PerformanceCounter drivePerfWriteCounter = new PerformanceCounter();

        public List<KeyValuePair<string, string>> GetDriveUsage()
        {
            //Get info for each drive on the system
            foreach (var item in drives)
            {
                //Make sure the drive is ready before trying to get info from it
                if (item.IsReady)
                {
                    dblDriveTotal = item.TotalSize / 1073741824.004733;
                    dblDriveSpace = item.AvailableFreeSpace / 1073741824.004733;
                    drivesList.Add(new KeyValuePair<string, string>(item.Name, dblDriveTotal.ToString() + "," + dblDriveSpace.ToString()));
                }
            }

            return drivesList;
        }

        public void GetDiskReadRate()
        {
            
            drivePerfReadCounter.CategoryName = "PhysicalDisk";
            drivePerfReadCounter.CounterName = "Disk Read Bytes/sec";
            drivePerfReadCounter.InstanceName = "_Total";

            dblRead = drivePerfReadCounter.NextValue() / 1000000;
        }

        public void GetDiskWriteRate()
        {
            drivePerfWriteCounter.CategoryName = "PhysicalDisk";
            drivePerfWriteCounter.CounterName = "Disk Write Bytes/sec";
            drivePerfWriteCounter.InstanceName = "_Total";

            dblWrite = drivePerfWriteCounter.NextValue() / 1000000;
        }

        public void DiskSpeedTest()
        {
            // Note: block size must be a factor of 1MB to avoid rounding errors :)
            const int blockSize = 1024 * 20;
            const int blocksPerMb = (2048 * 2048) / blockSize;
            byte[] data = new byte[blockSize];
            

            using (FileStream stream = File.OpenWrite(AppDomain.CurrentDomain.BaseDirectory + "test.txt"))
            {
                double averageWrite = dblWrite;
                for (int i = 0; i < 2048 * blocksPerMb; i++)
                {
                    stream.Write(data, 0, data.Length);
                    averageWrite = averageWrite + dblWrite;
                }

                averageWrite = averageWrite / (2048 * blocksPerMb);
            }

           // for (int i = 0; i < 5; i++)
           // {
                //using (FileStream stream = File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + "Readtest.txt"))
                //{
                //    byte[] buffer = new byte[1024 * 80];
                //    int c;
                //    string buffContainer;
                //    while ((c = stream.Read(buffer, 0, buffer.Length)) > 0)
                //    {
                //        buffContainer = Encoding.UTF8.GetString(buffer, 0, c);
                //    }
                    
                //    stream.Flush();
                //    stream.Close();
                //}
            //}
                

            File.Delete(AppDomain.CurrentDomain.BaseDirectory + "test.txt");
            //byte[] data = new byte[2048L * 1024 * 1024];
            //Random rng = new Random();
            //rng.zzNextBytes(data);
            //File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "test.txt", data);

            //File.Delete(AppDomain.CurrentDomain.BaseDirectory + "test.txt");
        }
    }
}
